#!/usr/bin/env bash
psql "postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST/$POSTGRES_DB?sslmode=disable" <<-EOSQL

CREATE TABLE "user" (
    id SERIAL PRIMARY KEY,
    first_name varchar(100),
    last_name varchar(100)
);

commit;

EOSQL