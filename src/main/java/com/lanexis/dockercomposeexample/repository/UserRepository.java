package com.lanexis.dockercomposeexample.repository;

import com.lanexis.dockercomposeexample.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {

    public List<User> findAllByLastName(String lastName);
}
