package com.lanexis.dockercomposeexample.controller;

import com.lanexis.dockercomposeexample.models.User;
import com.lanexis.dockercomposeexample.service.UserService;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Data
class UserRequest {
    private String firstName;
    private String lastName;
}

@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("add")
    ResponseEntity<String> addUser(@RequestBody UserRequest request) {
        User user = userService.createUser(request.getFirstName(),request.getLastName());
        return ResponseEntity.ok("Created User: " + user.toString());
    }

    @GetMapping("get")
    ResponseEntity<List<User>> getUserByLastName(@RequestParam String lastName) {
        return ResponseEntity.ok(userService.getUsersWithLastName(lastName));
    }

    @GetMapping("getAll")
    ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

}
