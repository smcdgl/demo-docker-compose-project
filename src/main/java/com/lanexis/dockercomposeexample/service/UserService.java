package com.lanexis.dockercomposeexample.service;

import com.lanexis.dockercomposeexample.models.User;
import com.lanexis.dockercomposeexample.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User createUser(String firstName, String lastName) {
        User createdUser = User.builder().firstName(firstName).lastName(lastName).build();
        return userRepository.save(createdUser);
    }

    public List<User> getUsersWithLastName(String lastName) {
        return userRepository.findAllByLastName(lastName);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
